import { Component, OnInit,Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-duvidas',
  templateUrl: './duvidas.page.html',
  styleUrls: ['./duvidas.page.scss'],
})
export class DuvidasPage extends BasePage implements OnInit {

 public projeto: any;
 public duvidas = [];
 public categoria: any;
 public cpf: any;
 public resposta: any;
 public duvida_selecionada = 0;
  constructor(injector: Injector,private activatedRoute: ActivatedRoute) { 
    super(injector);
  }

  ngOnInit() {
    this.nativeStorage.getItem('userMandato')
    .then(
        data => { this.cpf = data.cpf, this.categoria = data.categoriaTopino} ,
        error => console.error(error)
    );
    this.projeto = this.activatedRoute.snapshot.paramMap.get('projeto');
    this.carregar_duvidas(this.projeto);
  }

  cancelar_resposta()
  {
    this.duvida_selecionada = 0;
    this.resposta = "";
  }

  responder(duvida)
  {
    console.log("clicou ",duvida);
    if(this.categoria == "p30")
    {
      this.duvida_selecionada = duvida;
    }
  }

  enviar_resposta(id)
  {

    this.showLoader();
    this.http.post(this.url_service+"responder_duvida", {id: id, resposta: this.resposta}, {})
      .then(data => {
          const jsonData = JSON.parse(data.data);
          if(jsonData.status == "OK")
          {
              this.duvidas = jsonData.duvidas;
              this.duvida_selecionada = 0;
              this.resposta = "";
              this.hideLoader();  
              this.presentAlert("Sucesso","",jsonData.msg);
              //this.projetos
          }
          else
          {
              //this.loadingController.dismiss();
              this.hideLoader();
              this.presentAlert("Erro","",jsonData.msg);
          }
      })
      .catch(error => {
          //this.loadingController.dismiss();
          this.hideLoader();
          console.log("errore",error);
          this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
          
      });
  }

  carregar_duvidas(projeto)
  {

    this.showLoader();
    this.http.get(this.url_service+"retornar_duvidas", {projeto: projeto}, {})
      .then(data => {
          const jsonData = JSON.parse(data.data);
          if(jsonData.status == "OK")
          {
              //this.loadingController.dismiss();
              this.duvidas = jsonData.duvidas;
              this.hideLoader();  

              //this.presentAlert("Sucesso","",jsonData.msg);
              //this.projetos
          }
          else
          {
              //this.loadingController.dismiss();
              this.hideLoader();
              this.presentAlert("Erro","",jsonData.msg);
          }
      })
      .catch(error => {
          //this.loadingController.dismiss();
          this.hideLoader();
          console.log("errore",error);
          this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
          
      });
  }

}
