import { HTTP } from '@ionic-native/http/ngx';
import { ToastController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { BrMaskDirective, BrMaskModel } from 'br-mask';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Injector } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { ModalController, NavParams } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

export abstract class BasePage {
    public alertController: any;
    public loadingController: any;
    public loading: any;
    public toastController: any;
    public http: any;
    public nativeStorage: any;
    public formBuilder: any;
    public loaderToShow: any;
    public url_service = 'http://mandatopopular.herokuapp.com/users/';  //'http://8b0d5e0427b0.ngrok.io/users/' ; //
    public dom_sanitizer: any;
    public modal_controller: any;
    public nav_controller: any;
    public nav_extras: any;
    public router: any;
    public iab: InAppBrowser;
    //public nav_params: any;
    

    constructor(injector: Injector){
        this.alertController = injector.get(AlertController);
        this.loadingController = injector.get(LoadingController);
        this.toastController = injector.get(ToastController);
        this.http = injector.get(HTTP);
        this.nativeStorage = injector.get(NativeStorage);
        //this.formBuilder = injector.get(FormBuilder);
        this.dom_sanitizer = injector.get(DomSanitizer);
        this.modal_controller = injector.get(ModalController);
        this.nav_controller = injector.get(NavController);
        this.router = injector.get(Router);
        //this.nav_params = injector.get(NavParams);
        this.iab = injector.get(InAppBrowser);
        
    }

    async presentAlert(titulo,subtitulo,msg) {
        const alert = await this.alertController.create({
          header: titulo,
          subHeader: subtitulo,
          message: msg,
          buttons: ['OK']
        });
    
        await alert.present();
      }
    
    async presentLoading() {
        this.loading = await this.loadingController.create({
          message: 'Aguarde'
        });
       this.loading.present();
    
      
      }

      showLoader() {
        this.loaderToShow = this.loadingController.create({
          message: 'Aguarde'
        }).then((res) => {
          res.present();
    
          res.onDidDismiss().then((dis) => {
            console.log('Loading dismissed! after 2 Seconds');
          });
        });
        //this.hideLoader();
      }

      hideLoader() {
        setTimeout(() => {
          this.loadingController.dismiss();
        }, 4000);
      }
    
      navegar(url){
        this.router.navigateByUrl(url);
      }

      safe_url(url)
      {
        return this.dom_sanitizer.bypassSecurityTrustResourceUrl(url);
      }
    
    async presentToast(msg) {
            const toast = await this.toastController.create({
                message: msg,
                duration: 4000
            });
            toast.present();
        }
}