import { Component, OnInit,Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { DuvidasPage } from '../duvidas/duvidas.page';
import { NavigationExtras } from '@angular/router';


@Component({
  selector: 'app-projeto',
  templateUrl: './projeto.page.html',
  styleUrls: ['./projeto.page.scss'],
})
export class ProjetoPage extends BasePage implements OnInit {

  public nome: any;
  public descricao: any;
  public data_inicio: any;
  public data_termino: any;
  public id: any;
  public video: any;
  public categoria: any;
  public candidato: any;
  public projetos = [];
  public card_novo = false;
  public opcoes = [];
  public card_selecionado = 0;
  public duvida = "";
  public propor_projeto = false;
  public descricao_proposta: any;
  public url_social = "https://mandato-socify.herokuapp.com/posts/post_from_app";
  constructor(injector: Injector) {
    super(injector);
   }

  ngOnInit() {
    
  }

  propor_novo()
  {
    this.propor_projeto = !this.propor_projeto;
    this.descricao_proposta = "";
  }

  enviar_proposta()
  {
    this.showLoader();
    this.http.post(this.url_social, { id: this.id, content: this.descricao_proposta }, {})
      .then(data => {
          const jsonData = JSON.parse(data.data);
          if(jsonData.status == "ok")
          {
              this.descricao_proposta = "";
              this.propor_projeto = false;
              this.hideLoader();  
              this.presentAlert("Sucesso","",jsonData.msg);
          }
          else
          {
              this.hideLoader();
              this.presentAlert("Erro","",jsonData.msg);
          }
      })
      .catch(error => {
          this.hideLoader();
          console.log("errore",error);
          this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
          
      });
  }

  

  visualizar_projeto(id)
  {
    this.card_selecionado = id;
  }

  nova_opcao()
  {
    this.opcoes.push({opcao: ""});
  }

  ionViewWillEnter() {
    this.nativeStorage.getItem('userMandato')
    .then(
        data => { this.id = data.id, this.categoria = data.categoriaTopino, this.candidato = data.candidato
          this.carregar_projetos(this.categoria)
        } ,
        error => console.error(error)
    );
  }

  // async visualizar_duvidas(projeto)
  // {
  // console.log("abrir modal");

  //   const modal = await this.modal_controller.create({
  //     component: DuvidasPage,
  //     componentProps: {
  //       "projeto": projeto,
        
  //     }
  //   });

  //   modal.onDidDismiss().then((dataReturned) => {
      
  //   });

  //   return await modal.present();

  // }

  visualizar_duvidas(projeto)
  {
    this.router.navigate(['/duvidas/', projeto]);

  }

  carregar_projetos(tipo)
  {
    this.showLoader();
    this.http.get(this.url_service+"carregar_projetos", {id: this.id, tipo: tipo}, {})
      .then(data => {
          const jsonData = JSON.parse(data.data);
          if(jsonData.status == "OK")
          {
              //this.loadingController.dismiss();
              this.projetos = jsonData.projetos;
              this.hideLoader();  

              //this.presentAlert("Sucesso","",jsonData.msg);
              //this.projetos
          }
          else
          {
              //this.loadingController.dismiss();
              this.hideLoader();
              this.presentAlert("Erro","",jsonData.msg);
          }
      })
      .catch(error => {
          //this.loadingController.dismiss();
          this.hideLoader();
          console.log("errore",error);
          this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
          
      });
  }

  votar(op)
  {
    this.showLoader();
    this.http.post(this.url_service+"votar", {opcao: op, cpidf: this.id}, {})
      .then(data => {
          const jsonData = JSON.parse(data.data);
          if(jsonData.status == "OK")
          {
              this.hideLoader();  
              this.presentAlert("Sucesso","",jsonData.msg);
              //this.projetos
          }
          else
          {
              //this.loadingController.dismiss();
              this.hideLoader();
              this.presentAlert("Erro","",jsonData.msg);
          }
      })
      .catch(error => {
          //this.loadingController.dismiss();
          this.hideLoader();
          console.log("errore",error);
          this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
          
      });
  }

  enviar_duvida(projeto)
  {
    this.showLoader();
    this.http.post(this.url_service+"cadastrar_duvida", {duvida: this.duvida, projeto_id: projeto, id: this.id}, {})
      .then(data => {
          const jsonData = JSON.parse(data.data);
          if(jsonData.status == "OK")
          {
              this.duvida = "";
              this.hideLoader();  
              this.presentAlert("Sucesso","",jsonData.msg);

              //this.projetos
          }
          else
          {
              //this.loadingController.dismiss();
              this.hideLoader();
              this.presentAlert("Erro","",jsonData.msg);
          }
      })
      .catch(error => {
          //this.loadingController.dismiss();
          this.hideLoader();
          console.log("errore",error);
          this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
          
      });
  }
  

  novo_projeto()
  {
      this.card_novo = !this.card_novo;
  }

  salvar_projeto()
  {
    this.showLoader();
    this.http.post(this.url_service+"cadastrar_projeto", {opcoes: this.opcoes, nome: this.nome, id: this.id, descricao: this.descricao, data_inicio: this.data_inicio, data_termino: this.data_termino, video: this.video }, {})
      .then(data => {
          const jsonData = JSON.parse(data.data);
          if(jsonData.status == "OK")
          {
              //this.loadingController.dismiss();
              this.nome = "";
              this.descricao = "";
              this.video = "";
              this.data_inicio = "";
              this.data_termino = "";
              this.card_novo = false;
              this.opcoes = [];
              this.hideLoader();  

              this.presentAlert("Sucesso","",jsonData.msg);
              //this.projetos
          }
          else
          {
              //this.loadingController.dismiss();
              this.hideLoader();
              this.presentAlert("Erro","",jsonData.msg);
          }
      })
      .catch(error => {
          //this.loadingController.dismiss();
          this.hideLoader();
          console.log("errore",error);
          this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
          
      });
  }

}
