import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
//import { DuvidasPageModule } from '../duvidas/duvidas.module';

import { IonicModule } from '@ionic/angular';

import { ProjetoPage } from './projeto.page';
import { DuvidasPage } from '../duvidas/duvidas.page';



const routes: Routes = [
  {
    path: '',
    component: ProjetoPage
  },
  { path: 'duvidas/:projeto', loadChildren: '../duvidas/duvidas.module#DuvidasPageModule' }
];

@NgModule({
  imports: [
    
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    
    RouterModule.forChild(routes),
    //DuvidasPageModule,
  ],
  declarations: [ProjetoPage]
})
export class ProjetoPageModule {}
