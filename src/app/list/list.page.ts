import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController, IonSlides } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { ToastController } from '@ionic/angular';
import { IonInfiniteScroll } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {
  //@ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  private selectedItem: any;
  private icons = [
    'flask',
    'wifi',
    'beer',
    'football',
    'basketball',
    'paper-plane',
    'american-football',
    'boat',
    'bluetooth',
    'build'
  ];
  

  public wpUrl = 'http://mandatoblog.herokuapp.com/wp-json/wp/v2/posts';
  public wpPosts = [];
  public wpTotalPages: number;
  public wpTotalPosts: number;
  public paginaAtual = 1;
  public spinner = true;
  constructor(private iab: InAppBrowser,
    public toastController: ToastController,
     private http: HTTP,
     public menu: MenuController) {
    

    this.http.get(this.wpUrl+"?page="+this.paginaAtual, {}, {})
          .then(data => {
              this.wpPosts = JSON.parse(data.data);
              this.wpTotalPages = Number(data.headers["x-wp-totalpages"]);
              this.wpTotalPosts = Number(data.headers["x-wp-total"]);
              //this.presentToast("HEADER -"+JSON.stringify(data.headers));
              this.spinner = false;
          })
          .catch(error => {
              
            this.presentToast("ERROR - "+error.error);
          });
  }//fim contructor

  loadData(event) {
    if(this.paginaAtual < this.wpTotalPages)
    {
      setTimeout(() => {
        console.log('Done');
        this.paginaAtual += 1;
      
        this.http.get(this.wpUrl+"?page="+this.paginaAtual, {}, {})
            .then(data => {
                this.wpPosts.push.apply(this.wpPosts, JSON.parse(data.data));
            })
            .catch(error => {
                
              this.presentToast("ERROR - "+error.error);
            });
        event.target.complete();

        }, 500);
      }else{
        event.target.complete();
      }
  }

  openPost(link)
  {
      this.iab.create(link,"_system");
  }

  // toggleInfiniteScroll() {
  //   this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  // }

  async presentToast(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 4000
        });
        toast.present();
    }

  ionViewWillEnter() {
    this.menu.enable(true);
    //Busca os posts do WP
    
  }

  ngOnInit() {
  }
  // add back when alpha.4 is out
  // navigate(item) {
  //   this.router.navigate(['/list', JSON.stringify(item)]);
  // }
}
