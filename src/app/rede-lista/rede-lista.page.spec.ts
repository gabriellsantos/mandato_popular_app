import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedeListaPage } from './rede-lista.page';

describe('RedeListaPage', () => {
  let component: RedeListaPage;
  let fixture: ComponentFixture<RedeListaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedeListaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedeListaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
