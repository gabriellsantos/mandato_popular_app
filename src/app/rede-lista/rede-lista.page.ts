import { Component, OnInit,Injector } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-rede-lista',
  templateUrl: './rede-lista.page.html',
  styleUrls: ['./rede-lista.page.scss'],
})
export class RedeListaPage extends BasePage implements OnInit {

  public id: string;
  public membros: [];
  constructor(injector: Injector) {
    super(injector);
   }

  ngOnInit() {
    this.nativeStorage.getItem('userMandato')
    .then(
        data => {this.id = data.id
        this.buscar_rede()
      },
        error => console.error(error)
    );
  }

  buscar_rede()
  {
    this.showLoader();
    this.http.post(this.url_service+"buscar_membros", { id: this.id }, {})
    .then(data => {
      const jsonData = JSON.parse(data.data);
      if (jsonData.status == "OK") {
        this.membros = jsonData.membros;
        this.hideLoader();
      }
      else {
        this.hideLoader();
        this.presentAlert("Erro", "", "Não foi possível encontrar os membros");
      }
    })
    .catch(error => {
      this.hideLoader();
      this.presentAlert("Erro", "", "Não foi possível se conectar ao Servidor.");

    });
  }

}
