import { Component } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';
import { MenuController, IonSlides } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public nome: string;
  public categoria: string;
  public cpf: string;
  public id: any;
  public trustedVideoUrl: SafeResourceUrl;
  public has_notification = false;
  public wpUrl = 'http://mandatoblog.herokuapp.com/wp-json/wp/v2/posts';
  public wpPosts = [];
  constructor(private router: Router,private iab: InAppBrowser,private http: HTTP,public menu: MenuController,private nativeStorage: NativeStorage,private domSanitizer: DomSanitizer) {}

   ionViewWillEnter() {
    this.menu.enable(true);
    this.nativeStorage.getItem('has_notification').then(
        data => { this.has_notification = data.has_notification
          console.log("has notification",data)
          if(data.has_notification == true)
          {
            this.router.navigateByUrl('/notificacao');
          }
        },
        error => console.error(error)
      );
        this.nativeStorage.getItem('userMandato')
        .then(
            data => {this.id = data.id, this.cpf = data.cpf, this.nome = data.nome, this.categoria = data.categoriaNome},
            error => console.error(error)
        );

        this.http.get(this.wpUrl+"?page=1&per_page=1", {}, {})
        .then(data => {
            this.wpPosts = JSON.parse(data.data);
            //this.wpTotalPages = Number(data.headers["x-wp-totalpages"]);
            //this.wpTotalPosts = Number(data.headers["x-wp-total"]);
            //this.presentToast("HEADER -"+JSON.stringify(data.headers));
            //this.spinner = false;
        })
        .catch(error => {
            
          //this.presentToast("ERROR - "+error.error);
        });

    }

  openPost(link)
  {
      this.iab.create(link,"_system");
  }

  social() 
  {
    this.iab.create("https://mandato-socify.herokuapp.com/users/app_in?id=" + this.id, "_blank", "location=no,toolbar=yes,hardwareback=no");
  }
    

}
