import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { NativeStorage } from '@ionic-native/native-storage/ngx';

@Component({
  selector: 'app-notificacao',
  templateUrl: './notificacao.page.html',
  styleUrls: ['./notificacao.page.scss'],
})
export class NotificacaoPage implements OnInit {

  public titulo: string;
  public msg: string;
  constructor(private route: ActivatedRoute,private nativeStorage: NativeStorage) { 
    this.nativeStorage.getItem('notification').then(
      data => {this.titulo = data.title, this.msg = data.msg
      console.log("data - ",data.title)
      },
        error => console.error(error)
    );
    this.nativeStorage.setItem('has_notification', {has_notfication: false}).then(
        () => console.log('Stored item!'),
        error => console.error('Error storing item', error)
    );
  }

  ionViewWillEnter() {
    
        
  }

  ngOnInit() {
    

  }

}
