import { Component } from '@angular/core';

import { AlertController, Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

//Firebase
import { FirebaseX } from "@ionic-native/firebase-x/ngx";

import { AppVersion } from '@ionic-native/app-version/ngx';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Blog',
      url: '/list',
      icon: 'list-box'
    },
    {
      title: 'Indicar',
      url: '/indicar',
      icon: 'person-add'
    },
    {
      title: 'Meus Dados',
      url: '/meus-dados',
      icon: 'person'
    },
    {
      title: 'Minha Rede',
      url: '/rede',
      icon: 'people'
    },
    {
      title: 'Meu Candidato',
      url: '/candidato',
      icon: 'contact'
    }
    ,
    {
      title: 'Projetos',
      url: '/projeto',
      icon: 'clipboard'
    }
  ];

  public versao: any;
  public id: any;
  public categoria: any;
  constructor(
    private nativeStorage: NativeStorage,
    private iab: InAppBrowser,
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private alertCtrl: AlertController,
    private firebase: FirebaseX, private router: Router,
    public navCtrl: NavController,
    private appVersion: AppVersion
  ) {
    //this.versao = JSON.stringify(this.appVersion.getAppName());
    this.appVersion.getVersionNumber().then(value => {
      this.versao = value;
    }).catch(err => {
      //alert(err);
    });




    this.initializeApp();
  }


  incluir_menu() {
    this.nativeStorage.getItem('userMandato')
      .then(
        data => {
          this.id = data.id, this.categoria = data.categoriaTopino
          console.log("categoria - ", this.categoria);
          if (this.categoria == "p30") {
            this.appPages.push({
              title: 'Compartilhar Rede',
              url: '/compartilhar',
              icon: 'git-merge'
            });
          }
        },


        error => console.error(error)
      );
  }

  social() {
    this.nativeStorage.getItem('userMandato')
      .then(
        data => {
          this.id = data.id
          
          this.iab.create("https://mandato-socify.herokuapp.com/users/app_in?id=" + this.id, "_blank", "location=no,toolbar=yes,hardwareback=no");
        },
        error => console.error(error)
      );
  

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.solicitarTokenDoFirebase();
      this.incluir_menu();
    });
  }

  solicitarTokenDoFirebase() {

    this.firebase.getToken()
      .then(token => {
        console.log("firebase token recebido", token);
        //this.enviarTokenParaOservidor(token);
        //this.mostrarAlert("token", token);
        this.iniciarListenerDeNotificacoes();
      }) // save the token server-side and use it to push notifications to this device
      .catch(error => {
        console.error('Error getting token', error)
        //this.mostrarAlert("Error getting token", error);
      });

  }

  refreshToken() {
    this.firebase.onTokenRefresh().subscribe((token: string) => this.iniciarListenerDeNotificacoes());
  }

  
  iniciarListenerDeNotificacoes() {
    this.firebase.subscribe("geral");
    this.firebase.onMessageReceived().subscribe((notification: any) => {


      if (notification.tap) {
        console.log('notificacao -- ',notification);
        this.nativeStorage.setItem('notification', { title: notification.notification_title, msg: notification.notification_body }).then(
            () => console.log('Notificacao armazenada!', notification.title),
            error => console.error('Error storing item', error)
          );
        this.nativeStorage.setItem('has_notification', { has_notification: true }).then(
            () => console.log('Verificador armazenado!'),
            error => console.error('Error storing item', error)
          );
        
        this.router.navigateByUrl('/notificacao');
      }

    });

  }

  async mostrarAlert(titulo, texto) {
    const alert = await this.alertCtrl.create({
      header: titulo,
      message: texto,
      buttons: [
        {
          text: 'Fechar',
          handler: () => {
          }
        }
      ]
    });
    await alert.present();
  }
}
