import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatoPage } from './candidato.page';

describe('CandidatoPage', () => {
  let component: CandidatoPage;
  let fixture: ComponentFixture<CandidatoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
