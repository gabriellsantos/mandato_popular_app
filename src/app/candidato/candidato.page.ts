import { Component, OnInit } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { ToastController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { MenuController, IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicSelectableComponent } from 'ionic-selectable';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';



@Component({
  selector: 'app-candidato',
  templateUrl: './candidato.page.html',
  styleUrls: ['./candidato.page.scss'],
})
export class CandidatoPage implements OnInit {

  public url =  'http://mandatopopular.herokuapp.com/users/atualizar_dados'; //'http://34d24063c32a.ngrok.io/users/atualizar_dados'; //
  public loading: any;
  public texto_candidato: any;
  public video_candidato: any;
  public candidato: any;
  public categoriaTopino: any;
  public id: any;
  public exibir = false;
  public fotos = [];
  loaderToShow: any;
  

  //
  constructor(private photoViewer: PhotoViewer,public domSanitizer: DomSanitizer,public alertController: AlertController,public menu: MenuController,private router: Router,private http: HTTP,public toastController: ToastController,private nativeStorage: NativeStorage,public loadingController: LoadingController) { }

  ngOnInit() {
  }

  visualisar_foto(link)
  {
    console.log("url -",link);
    this.photoViewer.show(link);
  }

  mostrar_card()
  {
    this.exibir = !this.exibir
  }

  safeurl(url)
  {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(url);
  }

  atualizar() {
    //this.presentLoading();
    this.showLoader();

    this.http.post(this.url, {id: this.id, texto_candidato: this.texto_candidato, video_candidato: this.video_candidato}, {})
        .then(data => {
            const jsonData = JSON.parse(data.data);
            if(jsonData.status == "OK")
            {
                //this.loadingController.dismiss();
                this.hideLoader();  
                this.presentAlert("Sucesso","",jsonData.msg);
            }
            else
            {
                //this.loadingController.dismiss();
                this.hideLoader();
                this.presentAlert("Erro","",jsonData.msg);
            }
        })
        .catch(error => {
            //this.loadingController.dismiss();
            this.hideLoader();
            console.log("errore",error);
            this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
            
        });
  }

  ionViewWillEnter() {
    this.nativeStorage.getItem('userMandato')
    .then(
        data => { this.id = data.id, this.categoriaTopino = data.categoriaTopino, this.candidato = data.candidato, this.texto_candidato = data.texto_candidato, this.video_candidato = data.video_candidato
        this.carregar_fotos();
        } ,
        error => console.error(error)
    );
  }

  carregar_fotos()
  {
    this.showLoader();
    this.http.post("http://mandatopopular.herokuapp.com/users/carregar_fotos", {id: this.id}, {})
        .then(data => {
          
            const jsonData = JSON.parse(data.data);
            if(jsonData.status == "OK")
            {
              this.fotos = jsonData.fotos;
              console.log("fotos - ",this.fotos);
              this.hideLoader();  
                
            }
            else
            {
                this.hideLoader();
                this.presentAlert("Erro","","Erro ao carregar as fotos!");
            }
        })
        .catch(error => {
            //this.loadingController.dismiss();
            this.hideLoader();
            console.log("errore",error);
            this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
            
        });
  }

  showAutoHideLoader() {
    this.loadingController.create({
      message: 'This Loader Will Auto Hide in 2 Seconds',
      duration: 20000
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: 'Aguarde'
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
    //this.hideLoader();
  }

  hideLoader() {
    setTimeout(() => {
      this.loadingController.dismiss();
    }, 4000);
  }
  async presentAlert(titulo,subtitulo,msg) {
    const alert = await this.alertController.create({
      header: titulo,
      subHeader: subtitulo,
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Aguarde'
    });
    this.loading.present();

    const { role, data } = await this.loading.onDidDismiss();    
  
  }

    async presentToast(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 4000
        });
        toast.present();
    }


}
