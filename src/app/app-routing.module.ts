import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'list',
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'registro', loadChildren: './registro/registro.module#RegistroPageModule' },
  { path: 'indicar', loadChildren: './indicar/indicar.module#IndicarPageModule' },
  { path: 'notificacao', loadChildren: './notificacao/notificacao.module#NotificacaoPageModule' },
  { path: 'meus-dados', loadChildren: './meus-dados/meus-dados.module#MeusDadosPageModule' },
  { path: 'rede', loadChildren: './rede/rede.module#RedePageModule' },
  { path: 'candidato', loadChildren: './candidato/candidato.module#CandidatoPageModule' },
  { path: 'projeto', loadChildren: './projeto/projeto.module#ProjetoPageModule' },
  { path: 'duvidas/:projeto', loadChildren: './duvidas/duvidas.module#DuvidasPageModule' },
  { path: 'compartilhar', loadChildren: './compartilhar/compartilhar.module#CompartilharPageModule' },
  { path: 'membros', loadChildren: './membros/membros.module#MembrosPageModule' },
  { path: 'rede-lista', loadChildren: './rede-lista/rede-lista.module#RedeListaPageModule' }
];

//{ path: 'receber', loadChildren: './receber/receber.module#ReceberPageModule' },

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
