import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ToastController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-receber',
  templateUrl: './receber.page.html',
  styleUrls: ['./receber.page.scss'],
})
export class ReceberPage implements OnInit {

  public latitude: string;
public longitude: string;
public url = 'http://mandatopopular.herokuapp.com/users/receber_indicacao';
public cpf: string;
public loading: any;
  constructor(private nativeStorage: NativeStorage,
  private http: HTTP,
  private geolocation: Geolocation,
  public toastController: ToastController,
  public alertController: AlertController,
  public loadingController: LoadingController) { 

  }

  ngOnInit() {
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Aguarde'
    });
    this.loading.present();

  
  }

  async presentAlert(titulo,subtitulo,msg) {
    const alert = await this.alertController.create({
      header: titulo,
      subHeader: subtitulo,
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  ionViewWillEnter() {
        this.nativeStorage.getItem('userMandato')
        .then(
            data => {this.cpf = data.cpf},
            error => console.error(error)
        );
    }

  async presentToast(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 4000
        });
        toast.present();
    }

  receberIndicacao()
  {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = ""+resp.coords.latitude;
      this.longitude = ""+resp.coords.longitude;
      this.presentLoading();
      this.http.post(this.url, {cpf: this.cpf, latitude: this.latitude, longitude: this.longitude}, {})
          .then(data => {
              const jsonData = JSON.parse(data.data);
              if(jsonData.status == "OK")
              {
                this.loading.dismiss();
                
                    this.nativeStorage.setItem('userMandato', {cpf: jsonData.user.cpf, nome: jsonData.user.nome, categoria: jsonData.user.category_id,categoriaNome: jsonData.user.categoria_nome,categoriaTopino: jsonData.user.categoria_topico})
                    .then(
                        () => console.log('Stored item!'),
                        error => console.error('Error storing item', error)
                    );
                    this.presentAlert("Sucesso","","Parabéns agora você é "+jsonData.user.categoria_nome);
              }
              else
              {
                this.loading.dismiss();
                this.presentAlert("Erro","", jsonData.msg);
              }
          })
          .catch(error => {
            this.loading.dismiss();
              this.presentToast("Não foi possível se conectar ao Servidor.");
            
          });
    }).catch((error) => {
      this.loading.dismiss();
      this.presentToast("Não foi possível obter sua localização. Habilite seu GPS. "+error);
    });
  }

}
