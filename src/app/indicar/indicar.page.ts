import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ToastController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { BrMaskDirective, BrMaskModel } from 'br-mask';

@Component({
  selector: 'app-indicar',
  templateUrl: './indicar.page.html',
  styleUrls: ['./indicar.page.scss'],
})
export class IndicarPage implements OnInit {

public latitude: string;
public longitude: string; 
public url = 'http://mandatopopular.herokuapp.com/users'; //'http://c0465544fe48.ngrok.io/users'; 
public id: string;
public cpf_indicado: string;
public email: string;
public loading: any;
public indicar_op: null;
public categoria: any;
private formulario : FormGroup;
  constructor(private nativeStorage: NativeStorage,
  private http: HTTP,
  private geolocation: Geolocation,
  public toastController: ToastController,
  public alertController: AlertController,
  public loadingController: LoadingController,
  private formBuilder: FormBuilder
  ) { 
    this.formulario = this.formBuilder.group({
      cpf: ['', Validators.required],
      cpf_recuperar:['', Validators.required],
      email: ['', Validators.required],
      });
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Aguarde'
    });
    this.loading.present();

  
  }

  async presentAlert(titulo,subtitulo,msg) {
    const alert = await this.alertController.create({
      header: titulo,
      subHeader: subtitulo,
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
        this.nativeStorage.getItem('userMandato')
        .then(
            data => {this.id = data.id, this.categoria = data.categoriaTopino},
            error => console.error(error)
        );
    }

  async presentToast(msg) {
        const toast = await this.toastController.create({
            message: msg,
            duration: 4000
        });
        toast.present();
    }


  indicarBtn(op)
  {
    this.indicar_op = op;
  }  

  enviarIndicacao()
  {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = ""+resp.coords.latitude;
      this.longitude = ""+resp.coords.longitude;
      this.presentLoading();
      console.log("email - ",this.email);
      this.http.post(this.url+"/indicar", {email: this.email, id_indicador: this.id, latitude: this.latitude, longitude: this.longitude}, {})
          .then(data => {
              const jsonData = JSON.parse(data.data);
              if(jsonData.status == "OK")
              {
                this.loading.dismiss();
                this.presentAlert("Sucesso", "", jsonData.msg);
              }
              else
              {
                this.loading.dismiss();
                 this.presentAlert("Erro", "", jsonData.msg)
              }
          })
          .catch(error => {
            this.loading.dismiss();
              this.presentToast("Não foi possível se conectar ao Servidor.");
            
          });
    }).catch((error) => {
      this.loading.dismiss();
      this.presentToast("Não foi possível obter sua localização. Habilite seu GPS. "+error);
    });
  }

  enviarIndicacaoNaoMembro()
  {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = ""+resp.coords.latitude;
      this.longitude = ""+resp.coords.longitude;
      this.presentLoading();
      this.http.post(this.url+"/indicar_nao_membro", {id_indicador: this.id, email: this.email, latitude: this.latitude, longitude: this.longitude}, {})
          .then(data => {
              const jsonData = JSON.parse(data.data);
              if(jsonData.status == "OK")
              {
                this.loading.dismiss();
                this.presentAlert("Sucesso", "", jsonData.msg);
              }
              else
              {
                this.loading.dismiss();
                 this.presentAlert("Erro", "", jsonData.msg)
              }
          })
          .catch(error => {
            this.loading.dismiss();
              this.presentToast("Não foi possível se conectar ao Servidor.");
            
          });
    }).catch((error) => {
      this.loading.dismiss();
      this.presentToast("Não foi possível obter sua localização. Habilite seu GPS. "+error);
    });
  }

}
