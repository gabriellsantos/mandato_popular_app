import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicarPage } from './indicar.page';

describe('IndicarPage', () => {
  let component: IndicarPage;
  let fixture: ComponentFixture<IndicarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
