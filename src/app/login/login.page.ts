import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { MenuController, IonSlides } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { ToastController } from '@ionic/angular';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { BrMaskDirective, BrMaskModel } from 'br-mask';
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
//import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';







@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private formulario: FormGroup;
  public password: string;
  public cpf: string;
  public email: string;
  public url = 'http://mandatopopular.herokuapp.com/users/autenticar_via_app';//'http://1789fa9fc5a0.ngrok.io/users/autenticar_via_app';
  public url_senha = 'http://mandatopopular.herokuapp.com/users/recuperar_senha';
  public loading: any;
  public email_recuperar: string;
  public show_card = false;
  public loaderToShow: any;
  constructor(private formBuilder: FormBuilder, private firebase: FirebaseX, public loadingController: LoadingController, public alertController: AlertController, public toastController: ToastController, private http: HTTP, public menu: MenuController, private router: Router, private androidPermissions: AndroidPermissions, private nativeStorage: NativeStorage) {
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.INTERNET).then(
      result => console.log('Has permission?', result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.INTERNET)
    );

    this.formulario = this.formBuilder.group({
      email: ['', Validators.required],
      email_recuperar: ['', Validators.required],
    });
  }

  async presentAlert(titulo, subtitulo, msg) {
    const alert = await this.alertController.create({
      header: titulo,
      subHeader: subtitulo,
      message: msg,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Aguarde'
    });
    this.loading.present();


  }

  showLoader() {
    this.loaderToShow = this.loadingController.create({
      message: 'Aguarde'
    }).then((res) => {
      res.present();

      res.onDidDismiss().then((dis) => {
        console.log('Loading dismissed! after 2 Seconds');
      });
    });
    //this.hideLoader();
  }

  hideLoader() {
    setTimeout(() => {
      this.loadingController.dismiss();
    }, 4000);
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 4000
    });
    toast.present();
  }

  ngOnInit() {

    this.nativeStorage.getItem('userMandato')
      .then(
        data => {
          this.email = data.email, this.password = data.pwd
          if (data.pwd != "") {
            this.login();
          }
        },
        error => console.error(error)
      );

  }

  registro() {
    this.router.navigate(['/registro']);
  }

  show_card_senha() {
    this.show_card = !this.show_card;
  }

  recuperar_senha() {
    if (this.email_recuperar != "") {
      //this.presentLoading();
      this.showLoader();
      this.http.post(this.url_senha, { email: this.email_recuperar }, {})
        .then(data => {
          const jsonData = JSON.parse(data.data);
          if (jsonData.status == "OK") {
            //this.loading.dismiss();
            this.hideLoader();
            this.presentAlert("Sucesso", "", jsonData.msg);
          }
          else {
            //this.loading.dismiss();
            this.hideLoader();
            this.presentAlert("Erro", "", jsonData.msg);
          }
        })
        .catch(error => {
          //this.loading.dismiss();
          this.hideLoader();
          this.presentAlert("Erro", "", "Não foi possível se conectar ao Servidor.");

        });
    }
    else {
      this.presentAlert("Erro", "", "Informe o CPF.");
    }
  }

  login() {
    //this.presentLoading();
    this.showLoader();

    this.http.post(this.url, { password: this.password, email: this.email }, {})
      .then(data => {
        const jsonData = JSON.parse(data.data);
        if (jsonData.status == "OK") {
          //this.loading.dismiss();
          this.hideLoader();
          this.firebase.subscribe(jsonData.user.categoria_topico);
          this.firebase.subscribe(jsonData.user.cpf);
          this.firebase.subscribe(jsonData.user.id);
          this.firebase.subscribe("candidato_" + jsonData.user.cpf_candidato);
          this.firebase.subscribe(jsonData.token_notificacao)
          console.log("user - ", jsonData.user);
          this.nativeStorage.setItem('userMandato', { c30: jsonData.c30, pwd: this.password, texto_candidato: jsonData.texto_candidato, video_candidato: jsonData.video_candidato, candidato: jsonData.candidato, qtd_cidade: jsonData.qtd_cidade, qtd_abaixo: jsonData.qtd_abaixo, qtd_membros: jsonData.qtd_membros, estado: jsonData.user.estado, cidade_votacao: jsonData.user.cidade_votacao, estado_votacao: jsonData.user.estado_votacao, data_nascimento: jsonData.user.data_nascimento, email: jsonData.user.email, genero: jsonData.user.genero, telefone: jsonData.user.telefone, cidade: jsonData.user.cidade, cpf: jsonData.user.cpf, nome: jsonData.user.nome, categoria: jsonData.user.category_id,id: jsonData.user.id, categoriaNome: jsonData.user.categoria_nome, categoriaTopino: jsonData.user.categoria_topico })
            .then(
              () => console.log('Stored item!'),
              error => console.error('Error storing item', error)
            );
          this.router.navigateByUrl('/home');
        }
        else {
          //this.loading.dismiss();
          this.hideLoader();
          this.presentAlert("Erro", "", "Email ou Senha inválidos.");
        }
      })
      .catch(error => {
        this.nativeStorage.setItem('userMandato', { pwd: "", email: this.email })
          .then(
            () => console.log('Stored item!'),
            error => console.error('Error storing item', error)
          );
        //this.loading.dismiss();
        this.hideLoader();
        this.presentAlert("Erro", "", "Não foi possível se conectar ao Servidor.");

      });
  }

  ionViewWillEnter() {
    this.menu.enable(false);

  }

}
