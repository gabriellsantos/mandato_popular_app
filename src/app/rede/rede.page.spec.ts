import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RedePage } from './rede.page';

describe('RedePage', () => {
  let component: RedePage;
  let fixture: ComponentFixture<RedePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RedePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RedePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
