import { Component, OnInit,Injector } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';

import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-rede',
  templateUrl: './rede.page.html',
  styleUrls: ['./rede.page.scss'],
})
export class RedePage extends BasePage implements OnInit {

  public qtd_membros: any;
  public qtd_abaixo: any;
  public qtd_cidade: any; 
  public candidato: any;
  public c30 = [];
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }


  ionViewWillEnter() {
    this.nativeStorage.getItem('userMandato')
    .then(
        data => {this.c30 = data.c30, this.candidato = data.candidato,this.qtd_membros = data.qtd_membros, this.qtd_abaixo = data.qtd_abaixo, this.qtd_cidade = data.qtd_cidade} ,
        error => console.error(error)
    );
  }


  visualizar_rede()
  {
      this.navegar('/rede-lista');
  }

}
