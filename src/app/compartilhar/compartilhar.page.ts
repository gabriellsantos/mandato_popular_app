import { Component, OnInit, Injector } from '@angular/core';
import { IonicSelectableComponent } from 'ionic-selectable';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-compartilhar',
  templateUrl: './compartilhar.page.html',
  styleUrls: ['./compartilhar.page.scss'],
})
export class CompartilharPage extends BasePage implements OnInit {

  public id: any;
  public p30_selecionado: any;
  public p30: any;
  public compartilhado_id: 0;
  public compartilhado_nome: any;
  constructor(injector: Injector) 
  {
    super(injector);
  }

  ngOnInit() {
  }

  portChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    console.log('port:', event.value);
  }

  ionViewWillEnter() {
    this.nativeStorage.getItem('userMandato')
    .then(
        data => { this.id = data.id
          this.carregar_p30();
        } ,
        error => console.error(error)
    );

    this.nativeStorage.getItem('compartilhado')
    .then(
        data => { this.compartilhado_id = data.compartilhado_id, this.compartilhado_nome = data.compartilhado_nome
        console.log("compartilhado - ",this.compartilhado_id);
        } ,
        error => {
          this.compartilhado_id = 0
          console.error(error)}
    );
    
  }

  remover()
  {

    //this.presentLoading();
    this.showLoader();

    this.http.post(this.url_service+"remover_compartilhamento", {id: this.id}, {})
        .then(data => {
            const jsonData = JSON.parse(data.data);
            if(jsonData.status == "OK")
            {
              this.compartilhado_id = jsonData.compartilhado_id;
              this.compartilhado_nome = jsonData.compartilhado_nome;
              this.nativeStorage.setItem('compartilhado', {compartilhado_id: this.compartilhado_id, compartilhado_nome: this.compartilhado_nome})
                    .then(
                        () => console.log('Stored item!'),
                        error => console.error('Error storing item', error)
                    );
                //this.loadingController.dismiss();
                this.hideLoader();  
                this.presentAlert("Sucesso","",jsonData.msg);
            }
            else
            {
                //this.loadingController.dismiss();
                this.hideLoader();
                this.presentAlert("Erro","",jsonData.msg);
            }
        })
        .catch(error => {
            //this.loadingController.dismiss();
            this.hideLoader();
            console.log("errore",error);
            this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
            
        });
  }

  compartilhar()
  {

    //this.presentLoading();
    this.showLoader();

    this.http.post(this.url_service+"compartilhar_rede", {id: this.id, p30: this.p30_selecionado}, {})
        .then(data => {
            const jsonData = JSON.parse(data.data);
            if(jsonData.status == "OK")
            {
              this.compartilhado_id = jsonData.compartilhado_id;
              this.compartilhado_nome = jsonData.compartilhado_nome;
              this.nativeStorage.setItem('compartilhado', {compartilhado_id: this.compartilhado_id, compartilhado_nome: this.compartilhado_nome})
                    .then(
                        () => console.log('Stored item!'),
                        error => console.error('Error storing item', error)
                    );
                //this.loadingController.dismiss();
                this.hideLoader();  
                this.presentAlert("Sucesso","",jsonData.msg);
            }
            else
            {
                //this.loadingController.dismiss();
                this.hideLoader();
                this.presentAlert("Erro","",jsonData.msg);
            }
        })
        .catch(error => {
            //this.loadingController.dismiss();
            this.hideLoader();
            console.log("errore",error);
            this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
            
        });
  }

  carregar_p30()
  {
    this.showLoader();
    this.http.get(this.url_service+"carregar_p30", {id: this.id}, {})
      .then(data => {
          const jsonData = JSON.parse(data.data);
          if(jsonData.status == "OK")
          {
              //this.loadingController.dismiss();
              this.p30 = jsonData.p30;
              this.hideLoader();  

              //this.presentAlert("Sucesso","",jsonData.msg);
              //this.projetos
          }
          else
          {
              //this.loadingController.dismiss();
              this.hideLoader();
              this.presentAlert("Erro","",jsonData.msg);
          }
      })
      .catch(error => {
          //this.loadingController.dismiss();
          this.hideLoader();
          console.log("errore",error);
          this.presentAlert("Error","","Não foi possível se conectar ao Servidor.");
          
      });
  }

}
